package com.felixcool98.mods.classloader;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.collections.tuple.Tuple2;
import com.felixcool98.logging.Logs;
import com.felixcool98.logging.logtype.LogType;
import com.felixcool98.mods.Init;
import com.felixcool98.mods.Mod;
import com.felixcool98.mods.ModConfig;
import com.felixcool98.mods.ModFetcher;
import com.felixcool98.mods.ModInstance;
import com.felixcool98.mods.ModLoader;
import com.felixcool98.mods.utils.InstanciatedClass;

public class ClassModFetcher implements ModFetcher {
	private List<Class<?>> classes;
	
	
	public ClassModFetcher(List<Class<?>> classes) {
		this.classes = classes;
	}
	
	
	@Override
	public List<Tuple2<ModConfig, ?>> fetchMods() {
		List<Tuple2<ModConfig, ?>> data = new LinkedList<Tuple2<ModConfig,?>>();
		
		for(Class<?> classs : classes) {
			Tuple2<ModConfig, ClassModLoadData> tuple = load(classs);
			
			if(tuple == null)
				continue;
			
			data.add(tuple);
		}
		
		return data;
	}
	
	private Tuple2<ModConfig, ClassModLoadData> load(Class<?> classs){
		if(!classs.isAnnotationPresent(Mod.class)) {
			System.err.println("no @Mod annotation for "+classs);
			
			return null;
		}
		
		Mod mod = classs.getAnnotation(Mod.class);
		
		List<InstanciatedClass> classes = new LinkedList<InstanciatedClass>();
		
		if(classs.isAnnotationPresent(Init.class))
			classes.add(new InstanciatedClass(classs));
		
		for(Class<?> clasz : mod.classes()) {
			if(classs.equals(clasz)) {
				Logs.log(LogType.WARNING, clasz.toString()+" is mentioned in it's own @Mod annotation");
				continue;
			}
			
			if(!clasz.isAnnotationPresent(Init.class)) {
				Logs.log(LogType.WARNING, clasz.toString()+" has no @Init annotation");
				continue;
			}
			
			classes.add(new InstanciatedClass(clasz));
		}
		
		ModConfig config = new ModConfig(mod.name(), mod.dependencies());
		
		ClassModLoadData loadData = new ClassModLoadData(mod.resourcePackage());
		
		loadData.getClasses().addAll(classes);
		
		return new Tuple2<ModConfig, ClassModLoadData>(config, loadData);
	}
	
	@Override
	public ModLoader createModLoader(ModInstance mod) {
		if(!mod.getLoadData().contains(ClassModLoadData.class))
			return null;
		
		return new ClassModLoader(mod);
	}
}
