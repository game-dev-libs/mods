package com.felixcool98.mods.classloader;

public class ClassModException extends RuntimeException {
	private static final long serialVersionUID = -662046988748619988L;

	
	public ClassModException(String msg) {
		super(msg);
	}
}
