package com.felixcool98.mods.classloader;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.mods.utils.InstanciatedClass;

public class ClassModLoadData {
	private List<InstanciatedClass> classes = new LinkedList<InstanciatedClass>();
	
	private String ressourcePackage;
	
	
	public ClassModLoadData(String ressourcePackage) {
		this.ressourcePackage = ressourcePackage;
	}


	public List<InstanciatedClass> getClasses() {
		return classes;
	}
	
	
	public String getRessourcePackage() {
		return ressourcePackage;
	}
}
