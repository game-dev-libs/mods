package com.felixcool98.mods.jarloader;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.events.EventManager;
import com.felixcool98.mods.ModInstance;
import com.felixcool98.mods.ModLoader;
import com.felixcool98.mods.suppliers.LoadEventSupplier;
import com.felixcool98.mods.suppliers.registry.RegistrySupplier;
import com.felixcool98.mods.utils.ClassModFileResolver;
import com.felixcool98.mods.utils.FieldReader;
import com.felixcool98.mods.utils.InstanciatedClass;
import com.felixcool98.mods.utils.ModFileResolver;

public class JarModLoader implements ModLoader{
	private ModInstance mod;
	
	private EventManager eventManager = EventManager.DefaultEventManager();
	
	private JarModLoadData loadData;
	
	private ModFileResolver resolver;
	
	
	public JarModLoader(ModInstance mod) {
		this.mod = mod;
		this.loadData = mod.getLoadData().get(JarModLoadData.class);
		resolver = new ClassModFileResolver(loadData.getRessourcePackage());
	}
	
	
	@Override
	public void preload() {
		for(InstanciatedClass classs : loadData.getClasses()) {
			eventManager.listen(classs.getInstance());
		}
	}
	
	
	@SuppressWarnings("rawtypes")
	@Override
	public void load(LoadEventSupplier<?> supplier) {
		eventManager.send(supplier.createEvent(this));
		
		if(supplier instanceof RegistrySupplier)
			findObjectsInFields(((RegistrySupplier) supplier).getRegistryClass());
	}
	
	private void findObjectsInFields(Class<?> registryClass) {
		for(InstanciatedClass classs : loadData.getClasses()) {
			new FieldReader(getMod()).read(registryClass, classs.getClasss());
		}
	}
	
	@Override
	public ModInstance getMod() {
		return mod;
	}
	
	@Override
	public EventManager getEventManager() {
		return eventManager;
	}
	@Override
	public FileHandle resolve(FileHandle from, String path) {
		return resolver.resolve(from, path);
	}
	@Override
	public FileHandle resolve(String path) {
		return resolver.resolve(path);
	}
}
