package com.felixcool98.mods.jarloader;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.mods.utils.InstanciatedClass;

public class JarModLoadData {
	private List<InstanciatedClass> classes = new LinkedList<InstanciatedClass>();
	
	private String ressourcePackage;
	
	
	public JarModLoadData(String ressourcePackage) {
		this.ressourcePackage = ressourcePackage;
	}
	

	public List<InstanciatedClass> getClasses() {
		return classes;
	}
	
	public String getRessourcePackage() {
		return ressourcePackage;
	}
}
