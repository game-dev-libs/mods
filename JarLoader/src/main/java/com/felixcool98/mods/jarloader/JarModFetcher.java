package com.felixcool98.mods.jarloader;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.collections.tuple.Tuple2;
import com.felixcool98.mods.Init;
import com.felixcool98.mods.Mod;
import com.felixcool98.mods.ModConfig;
import com.felixcool98.mods.ModFetcher;
import com.felixcool98.mods.ModInstance;
import com.felixcool98.mods.ModLoader;
import com.felixcool98.mods.utils.InstanciatedClass;
import com.felixcool98.utility.values.Value;

public class JarModFetcher implements ModFetcher {
	private FileHandle dir;
	

	public JarModFetcher(FileHandle dir) {
		this.dir = dir;
	}
	

	@Override
	public List<Tuple2<ModConfig, ?>> fetchMods() {
		List<Tuple2<ModConfig, ?>> data = new LinkedList<Tuple2<ModConfig, ?>>();
		
		for(FileHandle handle : dir.list()) {
			if(!handle.extension().equals("jar"))
				continue;
			
			Tuple2<ModConfig, JarModLoadData> tuple = loadJar(handle);
			
			if(tuple != null)
				data.add(tuple);
		}
	
		return data;
	}
	private Tuple2<ModConfig, JarModLoadData> loadJar(FileHandle handle){
		List<InstanciatedClass> classes = new LinkedList<InstanciatedClass>();
		
		Value<Class<?>> configClass = new Value<Class<?>>();
		
		try {
			JarFile jar = new JarFile(handle.file());
			
			URL[] urls = { new URL("jar:file:" + handle.path()+"!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);
			
			jar.stream().filter((JarEntry entry)->{
				return entry.getName().endsWith("class");
			}).forEach((JarEntry entry) -> {
				String className = entry.getName().substring(0, entry.getName().length() - 6);
			    className = className.replace('/', '.');
			    
			    try {
					Class<?> classs = cl.loadClass(className);
					
					if(classs.isAnnotationPresent(Init.class)) {
						classes.add(new InstanciatedClass(classs));
					}
					if(classs.isAnnotationPresent(Mod.class)) {
						if(configClass.get() != null)
							throw new JarModException("multiple mod annotations found in jar");
						
						configClass.set(classs);
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			});
			
			jar.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(configClass.get() == null) {
			System.err.println("no @Mod annotation found for " + handle.name());
			
			//check for mod.info file
			
			return null;
		}
		
		Mod mod = configClass.get().getAnnotation(Mod.class);
		ModConfig config = new ModConfig(mod.name(), mod.dependencies());
		
		JarModLoadData data = new JarModLoadData(mod.resourcePackage());
		data.getClasses().addAll(classes);
		
		return new Tuple2<ModConfig, JarModLoadData>(config, data);
	}
	

	@Override
	public ModLoader createModLoader(ModInstance mod) {
		if(!mod.getLoadData().contains(JarModLoadData.class))
			return null;
		
		return new JarModLoader(mod);
	}
}
