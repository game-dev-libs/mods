package com.felixcool98.mods.jarloader;

public class JarModException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 962922054657153468L;

	
	public JarModException(String message) {
		super(message);
	}
}
