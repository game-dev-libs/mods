package com.felixcool98.mods.suppliers;

import java.util.List;

import com.felixcool98.dependencies.Dependency;
import com.felixcool98.dependencies.DependencyManager;
import com.felixcool98.logging.Logs;
import com.felixcool98.mods.GlobalRegistry;
import com.felixcool98.mods.ModLoader;
import com.felixcool98.mods.suppliers.registry.generic.GenericRegistryBuilder;

public class LoadEventSuppliers {
	private DependencyManager<LoadEventSupplier<?>> manager = new DependencyManager<LoadEventSupplier<?>>();
	
	
	public LoadEventSuppliers() {
		
	}
	
	
	public void register(LoadEventSupplier<?> supplier) {
		Dependency<LoadEventSupplier<?>> dependency = manager.wrap(supplier.getName(), supplier);
		dependency.setPriority(supplier.priority());
		dependency.addAll(supplier.dependencies());
		
		Logs.log(RegistryLogTypes.REGISTER_SUPPLIER, supplier.toString());
	}
	
	
	public LoadEventSupplierSupplier createSupplier() {
		return new LoadEventSupplierSupplier();
	}
	
	
	public List<LoadEventSupplier<?>> getAll(){
		return manager.getObjects();
	}
	
	
	public class LoadEventSupplierSupplier implements LoadEventSupplier<LoadEventSupplierRegistryEvent>{
		@Override
		public String[] dependencies() {
			return new String[0];
		}
		@Override
		public LoadEventSupplierRegistryEvent createEvent(ModLoader mod) {
			return new LoadEventSupplierRegistryEvent();
		}
		@Override
		public String getName() {
			return "suppliers";
		}
		
		@Override
		public String toString() {
			return "LoadEventSupplierRegistry";
		}
		
	}
	public class LoadEventSupplierRegistryEvent{
		public void register(LoadEventSupplier<?> supplier) {
			GlobalRegistry.REGISTRY.register(supplier);
		}
		
		public void registerAll(Class<?>...classes) {
			for(Class<?> classs : classes) {
				register(classs);
			}
		}
		public <T> void register(Class<T> classs) {
			register(new GenericRegistryBuilder<T>(classs).findDependenciesViaAnnotation().build());
		}
	}
}
