package com.felixcool98.mods.suppliers;

import com.felixcool98.logging.logtype.LogType;

public class RegistryLogTypes {
	public static final LogType REGISTER_OBJECT = new LogType("REGISTER", "OBJECT");
	public static final LogType REGISTER_SUPPLIER = new LogType("REGISTER", "SUPPLIER");
	public static final LogType REGISTER_REGISTRY = new LogType("REGISTER", "REGISTRY");
}
