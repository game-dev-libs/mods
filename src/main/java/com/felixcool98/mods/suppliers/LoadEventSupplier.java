package com.felixcool98.mods.suppliers;

import com.felixcool98.mods.ModLoader;

public interface LoadEventSupplier<T> {
	public default int priority() {
		return 0;
	}
	
	public String[] dependencies();
	public T createEvent(ModLoader loader);
	public String getName();
	
	public default String dependenciesToString() {
		String str = "{";
		
		for (int i = 0; i < dependencies().length; i++) {
			String dependency = dependencies()[i];
			
			str += dependency;
			if(i < dependencies().length-1)
				str += ",\n";
		}
		
		return str+"}";
	}
	
	public default Class<?>[] fieldsToRegister(){return new Class<?>[0];};
}
