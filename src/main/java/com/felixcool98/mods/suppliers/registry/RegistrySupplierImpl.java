package com.felixcool98.mods.suppliers.registry;

import com.felixcool98.mods.utils.ModUtils;

public abstract class RegistrySupplierImpl<T, A> implements RegistrySupplier<T, A>{
	private Registry<T> registry;
	private String[] dependencies;
	
	
	public RegistrySupplierImpl(Registry<T> registry) {
		this(registry, ModUtils.fetchDependencies(registry.getRegistryClass()));
	}
	public RegistrySupplierImpl(Registry<T> registry, String[] dependencies) {
		this.registry = registry;
		
		this.dependencies = dependencies;
	}
	public RegistrySupplierImpl(Registry<T> registry, Class<?>[] classDependencies, String[] dependencies) {
		this.registry = registry;
		
		this.dependencies = new String[classDependencies.length+dependencies.length];
		
		for(int i = 0; i < classDependencies.length; i++) {
			this.dependencies[i] = classDependencies[i].getCanonicalName();
		}
		for(int i = 0; i < dependencies.length; i++) {
			this.dependencies[i+classDependencies.length] = dependencies[i];
		}
	}
	
	
	@Override
	public String getName() {
		return getRegistryClass().getCanonicalName();
	};
	@Override
	public String[] dependencies() {
		return dependencies;
	}
	@Override
	public Class<T> getRegistryClass() {
		return registry.getRegistryClass();
	}
	@Override
	public Registry<T> getRegistry() {
		return registry;
	}
	
	@Override
	public String toString() {
		return getName();
	}
}