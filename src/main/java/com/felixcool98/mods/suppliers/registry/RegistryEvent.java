package com.felixcool98.mods.suppliers.registry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.events.GenericEvent;
import com.felixcool98.mods.ModLoader;

public class RegistryEvent<A, T extends Registry<A>> implements GenericEvent<A> {
	private ModLoader loader;
	private Registry<A> registry;
	
	
	public RegistryEvent(ModLoader loader, Registry<A> registry) {
		this.loader = loader;
		this.registry = registry;
	}
	
	
	public FileHandle getModFolder() {
		FileHandle handle = Gdx.files.local("mods/"+loader.getMod().getConfig().name);
		
		handle.mkdirs();
		
		return handle;
	}
	
	
	public FileHandle resolve(String path) {
		return loader.resolve(path);
	}
	
	
	public ModLoader getModLoader() {
		return loader;
	}
	public Registry<A> getRegistry() {
		return registry;
	}
	
	@Override
	public Class<A> getType() {
		return registry.getRegistryClass();
	}
}
