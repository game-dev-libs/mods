package com.felixcool98.mods.suppliers.registry;

import com.felixcool98.mods.suppliers.LoadEventSupplier;

/**
 * @author felixcool98
 */
public interface RegistrySupplier<T, A> extends LoadEventSupplier<A> {
	public Class<T> getRegistryClass();
	
	public Registry<T> getRegistry();
	
	public default boolean hasDependencies() {
		return dependencies().length != 0;
	}
	
	
	@Override
	default Class<?>[] fieldsToRegister() {
		return new Class<?>[] {getRegistryClass()};
	}
}
