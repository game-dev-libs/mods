package com.felixcool98.mods.suppliers.registry.generic;

import java.util.Collection;

import com.felixcool98.collections.tuple.Tuple2;
import com.felixcool98.mods.ModLoader;
import com.felixcool98.mods.suppliers.registry.RegistryEvent;
import com.felixcool98.mods.utils.ObjectPath;

public class GenericRegistryEvent<T> extends RegistryEvent<T, GenericRegistry<T>>{
	public GenericRegistryEvent(ModLoader loader, GenericRegistry<T> registry) {
		super(loader, registry);
	}
	
	
	public void register(String path, T object) {
		register(new ObjectPath(getModLoader().getMod().getConfig().name, path), object);
	}
	
	public void register(Collection<Tuple2<ObjectPath, T>> tuples) {
		for(Tuple2<ObjectPath, T> tuple : tuples)
			register(tuple);
	}
	public void register(Tuple2<ObjectPath, T> tuple) {
		register(tuple.a, tuple.b);
	}
	
	public void register(ObjectPath path, T object) {
		getRegistry().register(path, object);
	}
	
	
	@Override
	public GenericRegistry<T> getRegistry() {
		return (GenericRegistry<T>) super.getRegistry();
	}
}