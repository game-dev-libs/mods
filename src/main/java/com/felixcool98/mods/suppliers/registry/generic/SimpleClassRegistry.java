package com.felixcool98.mods.suppliers.registry.generic;

public class SimpleClassRegistry<T> extends AbstractRegistry<T>{
	private final Class<T> registryClass;
	
	
	public SimpleClassRegistry(Class<T> registryClass) {
		this.registryClass = registryClass;
	}
	
	
	@Override
	public Class<T> getRegistryClass() {
		return registryClass;
	}
}
