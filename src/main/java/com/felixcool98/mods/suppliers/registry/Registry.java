package com.felixcool98.mods.suppliers.registry;

public interface Registry<T> {
	public Class<T> getRegistryClass();
}
