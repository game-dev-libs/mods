package com.felixcool98.mods.suppliers.registry.generic;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.mods.DependsOn;
import com.felixcool98.mods.ModLoader;
import com.felixcool98.mods.suppliers.registry.RegistrySupplier;
import com.felixcool98.mods.suppliers.registry.RegistrySupplierImpl;

public class GenericRegistryBuilder<T> {
	private GenericRegistry<T> registry;
	
	private List<String> dependencies = new LinkedList<String>();
	
	
	public GenericRegistryBuilder(Class<T> classs) {
		this(new SimpleClassRegistry<T>(classs));
	}
	public GenericRegistryBuilder(GenericRegistry<T> registry) {
		this.registry = registry;
	}
	
	
	public GenericRegistryBuilder<T> addDependency(Class<?> classs){
		dependencies.add(classs.getCanonicalName());
		
		return this;
	}
	public GenericRegistryBuilder<T> findDependenciesViaAnnotation(){
		DependsOn dependencies = registry.getClass().getAnnotation(DependsOn.class);
		
		if(dependencies == null)
			 dependencies = registry.getRegistryClass().getAnnotation(DependsOn.class);
		
		if(dependencies != null) {
			for(Class<?> dependency : dependencies.classDependencies()) {
				this.dependencies.add(dependency.getCanonicalName());
			}
			for(String str : dependencies.dependencies()) {
				this.dependencies.add(str);
			}
		}
		
		return this;
	}
	
	
	public RegistrySupplier<T, GenericRegistryEvent<T>> build(){
		return new RegistrySupplierImpl<T, GenericRegistryEvent<T>>(registry, dependencies.toArray(new String[0])) {
			@Override
			public GenericRegistryEvent<T> createEvent(ModLoader loader) {
				return new GenericRegistryEvent<T>(loader, registry);
			}
		};
	}
}
