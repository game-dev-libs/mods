package com.felixcool98.mods.suppliers.registry.generic;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.felixcool98.logging.Logs;
import com.felixcool98.mods.suppliers.RegistryLogTypes;
import com.felixcool98.mods.utils.ObjectPath;

public abstract class AbstractRegistry<T> implements GenericRegistry<T>{
	private Map<ObjectPath, T> objects = new HashMap<ObjectPath, T>();
	
	
	@Override
	public void register(ObjectPath path, T object) {
		objects.put(path, object);
		
		Logs.log(RegistryLogTypes.REGISTER_OBJECT, "(Type) "+getRegistryClass()+" (Name) "+path+" (Object) "+object);
	}
	
	@Override
	public T get(ObjectPath path) {
		return objects.get(path);
	}
	@Override
	public List<T> get(String match) {
		List<T> list = new LinkedList<T>();
		
		final MatchType type;
		
		if(!match.contains(":"))
			type = MatchType.Name;
		else if(match.endsWith(":"))
			type = MatchType.Mod;
		else
			type = MatchType.ModAndName;
		
		objects.entrySet().stream().filter(new Predicate<Entry<ObjectPath, T>>() {
			@Override
			public boolean test(Entry<ObjectPath, T> entry) {
				switch (type) {
					case Name:
						if(entry.getKey().getPath().toLowerCase().contains(match.toLowerCase()))
							return true;
						
						return false;
						
					case Mod:
						if(entry.getKey().getMod().toLowerCase().contains(match.replaceAll(":", "").toLowerCase()))
							return true;
						
						return false;
						
					case ModAndName:
						ObjectPath path = ObjectPath.parseObjectPath(match);
						
						if(entry.getKey().getMod().toLowerCase().contains(path.getMod().toLowerCase()) && entry.getKey().getPath().toLowerCase().contains(path.getPath().toLowerCase()))
							return true;
						
						return false;
				}
				
				return false;
			}
		}).sorted((obj0, obj1)->{
			if(type == MatchType.Name) {
				if(obj0.getKey().getPath().equals(match) && obj1.getKey().getPath().equals(match))
					return 0;
				else if(obj0.getKey().getPath().equals(match))
					return -1;
				else
					return 1;
 			}
			
			
			return 0;
		}).forEach(new Consumer<Entry<ObjectPath, T>>() {
			@Override
			public void accept(Entry<ObjectPath, T> t) {
				synchronized (list) {
					list.add(t.getValue());
				}
			}
		});
		
		return list;
	}
	@Override
	public Collection<T> getAll() {
		return objects.values();
	}
	
	
	static enum MatchType{
		Mod,
		ModAndName,
		Name;
	}
}