package com.felixcool98.mods.suppliers.registry.generic;

import java.util.Collection;
import java.util.List;

import com.felixcool98.mods.suppliers.registry.Registry;
import com.felixcool98.mods.utils.ObjectPath;

public interface GenericRegistry<T> extends Registry<T> {
	public void register(ObjectPath path, T object);
	
	public T get(ObjectPath path);
	public List<T> get(String match);
	public Collection<T> getAll();
}
