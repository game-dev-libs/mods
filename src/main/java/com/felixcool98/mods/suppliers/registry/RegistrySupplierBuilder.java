package com.felixcool98.mods.suppliers.registry;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.mods.DependsOn;
import com.felixcool98.mods.ModLoader;
import com.felixcool98.mods.suppliers.registry.generic.SimpleClassRegistry;
import com.felixcool98.mods.utils.ModUtils;

public class RegistrySupplierBuilder<T> {
	private Registry<T> registry;
	
	private List<String> dependencies = new LinkedList<String>();
	
	
	public RegistrySupplierBuilder(Class<T> classs) {
		this(new SimpleClassRegistry<T>(classs));
	}
	public RegistrySupplierBuilder(Registry<T> registry) {
		this.registry = registry;
	}
	
	
	public RegistrySupplierBuilder<T> addDependency(Class<?> classs){
		dependencies.add(classs.getCanonicalName());
		
		return this;
	}
	public RegistrySupplierBuilder<T> findDependenciesViaAnnotation(){
		DependsOn dependencies = registry.getClass().getAnnotation(DependsOn.class);
		
		if(dependencies == null)
			 dependencies = registry.getRegistryClass().getAnnotation(DependsOn.class);
		
		if(dependencies != null) {
			for(String dependency : ModUtils.fetchDependencies(dependencies)) {
				this.dependencies.add(dependency);
			}
		}
		
		return this;
	}
	
	
	public RegistrySupplier<T, RegistryEvent<T, Registry<T>>> build(){
		return new RegistrySupplierImpl<T, RegistryEvent<T, Registry<T>>>(registry, dependencies.toArray(new String[0])) {
			@Override
			public RegistryEvent<T, Registry<T>> createEvent(ModLoader loader) {
				return new RegistryEvent<T, Registry<T>>(loader, registry);
			}
		};
	}
}
