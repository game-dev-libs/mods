package com.felixcool98.mods.suppliers.registry.generic;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.mods.DependsOn;
import com.felixcool98.mods.ModLoader;
import com.felixcool98.mods.suppliers.registry.Registry;
import com.felixcool98.mods.suppliers.registry.RegistrySupplier;
import com.felixcool98.mods.utils.ModUtils;

public class GenericRegistryEntry<T> implements RegistrySupplier<T, GenericRegistryEvent<T>> {
	private GenericRegistry<T> registry;
	
	private String[] dependencies;
	
	
	public GenericRegistryEntry(GenericRegistry<T> registry) {
		this(registry, new String[0]);
	}
	public GenericRegistryEntry(GenericRegistry<T> registry, String[] dependencies) {
		this.registry = registry;
		this.dependencies = dependencies;
	}
	
	
	@Override
	public String[] dependencies() {
		return dependencies;
	}
	
	
	@Override
	public String getName() {
		return getRegistryClass().getCanonicalName();
	}

	@Override
	public Class<T> getRegistryClass() {
		return registry.getRegistryClass();
	}

	
	@Override
	public GenericRegistryEvent<T> createEvent(ModLoader load) {
		return new GenericRegistryEvent<T>(load, registry);
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Registry) {
			if(getRegistryClass().equals(((Registry<?>) obj).getRegistryClass()))
				return true;
		}
		
		return false;
	}
	
	
	public GenericRegistry<T> getRegistry() {
		return registry;
	}
	
	
	@Override
	public String toString() {
		return getName();
	}
	
	
	public static class RegistrySupplierBuilder<T> {
		private GenericRegistry<T> registry;
		
		private List<String> dependencies = new LinkedList<String>();
		
		
		public RegistrySupplierBuilder(Class<T> classs) {
			this(new SimpleClassRegistry<T>(classs));
		}
		public RegistrySupplierBuilder(GenericRegistry<T> registry) {
			this.registry = registry;
		}
		
		
		public RegistrySupplierBuilder<T> addDependency(Class<?> classs){
			dependencies.add(classs.getCanonicalName());
			
			return this;
		}
		public RegistrySupplierBuilder<T> findDependenciesViaAnnotation(){
			DependsOn dependencies = registry.getClass().getAnnotation(DependsOn.class);
			
			if(dependencies == null)
				 dependencies = registry.getRegistryClass().getAnnotation(DependsOn.class);
			
			if(dependencies != null) {
				for(String dependency : ModUtils.fetchDependencies(dependencies)) {
					this.dependencies.add(dependency);
				}
			}
			
			return this;
		}
		
		
		public RegistrySupplier<T, GenericRegistryEvent<T>> build(){
			return new GenericRegistryEntry<T>(registry, dependencies.toArray(new String[0]));
		}
	}
}
