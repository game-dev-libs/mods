package com.felixcool98.mods.suppliers.registry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.felixcool98.logging.Logs;
import com.felixcool98.mods.suppliers.RegistryLogTypes;
import com.felixcool98.mods.suppliers.registry.generic.GenericRegistry;
import com.felixcool98.mods.utils.ObjectPath;

public class Registries {
	public static final Registries REGISTRIES = new Registries();
	
	
	private Map<Class<?>, Registry<?>> registries = new HashMap<Class<?>, Registry<?>>();
	
	
	public synchronized void register(Registry<?> registry) {
		registries.put(registry.getRegistryClass(), registry);
		
		Logs.log(RegistryLogTypes.REGISTER_REGISTRY, registry.toString());
	}
	@SuppressWarnings("unchecked")
	public synchronized <T> void register(ObjectPath path, T object) {
		GenericRegistry<T> registry = (GenericRegistry<T>) get(object.getClass());
		
		if(registry.get(path) != null && registry.get(path).equals(object)) 
			return;
		
		registry.register(path, object);
		
		Logs.log(RegistryLogTypes.REGISTER_OBJECT, object.getClass().getName() + " | " + path);
	}
	
	
	//=================================================
	// get
	//=================================================
	
	public <T> T get(ObjectPath path, Class<T> classs) {
		return get(classs).get(path);
	}
	public <T> List<T> get(String match, Class<T> classs) {
		return get(classs).get(match);
	}
	/**
	 * unsafe only use if you know that the registry registered on the class is a generic registry
	 * @param <T>
	 * @param classs
	 * @return
	 */
	public <T> GenericRegistry<T> get(Class<T> classs){
		return (GenericRegistry<T>) getRegistry(classs);
	}
	@SuppressWarnings("unchecked")
	public <T> Registry<T> getRegistry(Class<T> classs){
		return (Registry<T>) registries.get(classs);
	}
}
