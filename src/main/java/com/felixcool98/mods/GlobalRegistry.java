package com.felixcool98.mods;

import com.felixcool98.mods.suppliers.LoadEventSupplier;
import com.felixcool98.mods.suppliers.LoadEventSuppliers;
import com.felixcool98.mods.suppliers.registry.Registries;
import com.felixcool98.mods.suppliers.registry.RegistrySupplier;

public class GlobalRegistry {
	public static final GlobalRegistry REGISTRY = new GlobalRegistry();
	
	public final Registries registries = Registries.REGISTRIES;
	
	public final LoadEventSuppliers suppliers = new LoadEventSuppliers();
	
	
	//=====================================================
	// register
	//=====================================================
	
	@SuppressWarnings("rawtypes")
	public synchronized void register(LoadEventSupplier<?> supplier) {
		suppliers.register(supplier);
		
		if(supplier instanceof RegistrySupplier) {
			registries.register(((RegistrySupplier) supplier).getRegistry());
		}
	}
}
