package com.felixcool98.mods;

public class ModInstance {
	private ModConfig config;
	
	private ModLoadData loadData = new ModLoadData();
	
	private String errorLog = "";
	
	
	/**
	 * @param name
	 * @param dependencies
	 * @param dir
	 * @param resourcePackage
	 */
	public ModInstance(ModConfig config) {
		this.config = config;
	}
	
	
	public ModConfig getConfig() {
		return config;
	}
	
	public ModLoadData getLoadData() {
		return loadData;
	}
	
	
	public String getErrorLog() {
		return errorLog;
	}
	
	public void appendToErrorLog(String text) {
		errorLog += text;
	}
	
	
	@Override
	public String toString() {
		return getConfig().name;
	}
}
