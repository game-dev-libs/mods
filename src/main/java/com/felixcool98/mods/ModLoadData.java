package com.felixcool98.mods;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ModLoadData {
	private Map<Class<?>, Object> data = new HashMap<Class<?>, Object>();
	
	private List<ModLoader> loaders = new LinkedList<ModLoader>();
	
	
	public <T> void add(T object) {
		data.put(object.getClass(), object);
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> type) {
		return (T) data.get(type);
	}
	
	
	public boolean contains(Class<?> type) {
		return data.containsKey(type);
	}
	
	
	public List<ModLoader> getLoaders() {
		return loaders;
	}
}
