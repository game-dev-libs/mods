package com.felixcool98.mods;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface Mod {
	/**
	 * name of the mod<br>
	 * must be unique<br>
	 * used to obtain objects from the mod
	 */
	public String name();
	/**
	 * all classes belonging to the mod that should be loaded<br>
	 * only needed for dev / debug builds
	 */
	public Class<?>[] classes() default {};
	public String[] dependencies()  default{};
	
	public String resourcePackage() default "";
	public String dir() default "";
}
