package com.felixcool98.mods.events;

import com.felixcool98.mods.ModLoader;

public class ModLoaderListenerRegistryEvent {
	private ModLoader loader;
	
	
	public ModLoaderListenerRegistryEvent(ModLoader loader) {
		this.loader = loader;
	}
	
	
	public ModLoader getLoader() {
		return loader;
	}
}
