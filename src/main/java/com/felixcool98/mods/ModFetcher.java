package com.felixcool98.mods;

import java.util.List;

import com.felixcool98.collections.tuple.Tuple2;

/**
 * creates {@link ModInstance}s and their respective {@link ModLoader}
 * 
 * @author felixcool98
 * @param T type of the additional load data provided by this fetcher
 */
public interface ModFetcher {
	/**
	 * mods created by lower priority fetchers will have their {@link ModConfig} overwritten by higher priorities
	 */
	public default int priority() {return 0;}
	
	/**
	 * fetches all {@link ModConfig}s and additional data needed for loading<br>
	 */
	public List<Tuple2<ModConfig, ?>> fetchMods();
	public ModLoader createModLoader(ModInstance mod);
}
