package com.felixcool98.mods.utils;

import com.badlogic.gdx.files.FileHandle;

public interface ModFileResolver {
	public FileHandle resolve(String path);
	
	/**
	 * if supported allows paths relative to the given filehandle
	 */
	public default FileHandle resolve(FileHandle from, String path) {
		return resolve(path);
	}
}
