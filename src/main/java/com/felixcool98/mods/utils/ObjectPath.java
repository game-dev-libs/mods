package com.felixcool98.mods.utils;

public class ObjectPath {
	private String mod;
	private String path;
	
	
	public ObjectPath(String mod, String path) {
		super();
		this.mod = mod;
		this.path = path;
	}


	public String getMod() {
		return mod;
	}


	public String getPath() {
		return path;
	}
	
	
	@Override
	public String toString() {
		return getMod()+":"+getPath();
	}
	@Override
	public int hashCode() {
		return toString().toLowerCase().hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ObjectPath) {
			return hashCode() == obj.hashCode();
		}
		
		return false;
	}
	
	
	public static ObjectPath parseObjectPath(String string) {
		String mod = string.substring(0, string.indexOf(":"));
		String path = string.substring(string.indexOf(":")+1, string.length());
		
		return new ObjectPath(mod, path);
	}
}
