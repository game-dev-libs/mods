package com.felixcool98.mods.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class ClassModFileResolver implements ModFileResolver {
	private String resourcePackage;
	
	
	public ClassModFileResolver(String resourcePackage) {
		this.resourcePackage = resourcePackage;
	}


	@Override
	public FileHandle resolve(String path) {
		return resPackage().child(path);
	}
	
	
	private FileHandle resPackage() {
		return Gdx.files.classpath(resourcePackage.replaceAll("\\.", "/"));
	}
}
