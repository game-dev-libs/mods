package com.felixcool98.mods.utils;

import com.felixcool98.mods.DependsOn;

public class ModUtils {
	public static String[] fetchDependencies(Class<?> classs){
		if(!classs.isAnnotationPresent(DependsOn.class))
			return new String[0];
		
		DependsOn d = classs.getAnnotation(DependsOn.class);
		
		return fetchDependencies(d);
	}
	public static String[] fetchDependencies(DependsOn d){
		String[] dependencies = new String[d.classDependencies().length+d.dependencies().length];
		
		for(int i = 0; i < d.classDependencies().length; i++) {
			dependencies[i] = d.classDependencies()[i].getCanonicalName();
		}
		for(int i = 0; i < d.dependencies().length; i++) {
			dependencies[i+d.classDependencies().length] = d.dependencies()[i];
		}
		
		return dependencies;
	}
	public static String[] convert(Class<?>... classes) {
		String[] dependencies = new String[classes.length];
		
		for(int i = 0; i < dependencies.length; i++) {
			dependencies[i] = classes[i].getCanonicalName();
		}
		
		return dependencies;
	}
}
