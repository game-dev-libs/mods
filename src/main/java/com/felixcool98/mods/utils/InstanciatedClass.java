package com.felixcool98.mods.utils;

import java.lang.reflect.InvocationTargetException;

public class InstanciatedClass{
	private Class<?> classs;
	private Object instance;
	
	
	public InstanciatedClass(Class<?> classs) {
		this.classs = classs;
		
		try {
			instance = classs.getConstructor().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
	
	
	public Class<?> getClasss() {
		return classs;
	}
	public Object getInstance() {
		return instance;
	}
}