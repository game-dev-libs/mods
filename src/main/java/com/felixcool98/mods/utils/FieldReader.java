package com.felixcool98.mods.utils;

import java.lang.reflect.Field;

import com.felixcool98.mods.GlobalRegistry;
import com.felixcool98.mods.ModInstance;
import com.felixcool98.mods.ObjectHolder;
import com.felixcool98.mods.suppliers.registry.generic.GenericRegistry;
import com.felixcool98.mods.utils.ObjectPath;

public class FieldReader {
	private ModInstance mod;
	
	
	public FieldReader(ModInstance mod) {
		this.mod = mod;
	}
	
	
	public void read(Class<?> registryClass, Class<?> read) {
		try {
			for(Field field : read.getFields()) {
				if(!registryClass.isAssignableFrom(field.getType()))
					continue;
				
				if(!java.lang.reflect.Modifier.isStatic(field.getModifiers()))
					continue;
				
				if(!field.isAnnotationPresent(ObjectHolder.class))
					continue;
				
				if(field.get(null) == null)
					continue;
				
				ObjectHolder holder = field.getAnnotation(ObjectHolder.class);
				
				String modName = holder.mod().isEmpty() ? mod.getConfig().name : holder.mod();
				String path = holder.path().isEmpty() ? field.getName() : holder.path();
				
				ObjectPath objPath = new ObjectPath(modName, path);
				
				register(registryClass, objPath, field.get(null));
				
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	private <T> void register(Class<?> classs, ObjectPath path, T object) {
		GenericRegistry<T> registry = (GenericRegistry<T>) GlobalRegistry.REGISTRY.registries.get(classs);
		
		registry.register(path, object);
	}
}
