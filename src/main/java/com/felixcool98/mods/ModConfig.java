package com.felixcool98.mods;

public class ModConfig {
	public final String name;
	public final String[] dependencies;
	
	public final int priority;
	
	
	public ModConfig(String name, String[] dependencies) {
		this(name, dependencies, 0);
	}
	public ModConfig(String name, String[] dependencies, int priority) {
		this.name = name;
		this.dependencies = dependencies;
		this.priority = priority;
	}
}
