package com.felixcool98.mods;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.felixcool98.collections.tuple.Tuple2;
import com.felixcool98.dependencies.Dependency;
import com.felixcool98.dependencies.DependencyManager;
import com.felixcool98.logging.Logs;
import com.felixcool98.logging.logtype.LogType;
import com.felixcool98.mods.events.ModLoaderListenerRegistryEvent;
import com.felixcool98.mods.suppliers.LoadEventSupplier;

/**
 * Main mod class<br>
 * loads mods
 * 
 * @author felixcool98
 */
public class Mods {
	public static final LogType REGISTRY = new LogType("REGISTRY");
	
	
	public static List<ModFetcher> fetchers = new LinkedList<ModFetcher>();
	
	public static Map<String, ModInstance> mods = new HashMap<String, ModInstance>();
	
	
	public static void start() {
		preload();
		load();
	}
	
	private static void preload() {
		Map<String, ModConfig> configs = new HashMap<String, ModConfig>();
		
		List<Tuple2<ModConfig, ?>> allData = new LinkedList<Tuple2<ModConfig, ?>>();
		
		for(ModFetcher fetcher : fetchers) {
			allData.addAll(fetcher.fetchMods());
		}
		
		for(Tuple2<ModConfig, ?> data : allData) {
			if(configs.containsKey(data.a.name)) {
				if(configs.get(data.a.name).priority >= data.a.priority)
					continue;
			}
			
			configs.put(data.a.name, data.a);
		}
		
		for(ModConfig config : configs.values()) {
			mods.put(config.name, new ModInstance(config));
		}
		
		for(Tuple2<ModConfig, ?> data : allData) {
			if(data.b == null)
				continue;
			
			mods.get(data.a.name).getLoadData().add(data.b);
		}
	}
	
	private static void load() {
		for(ModInstance mod : mods.values()) {
			for(ModFetcher fetcher : fetchers) {
				ModLoader loader = fetcher.createModLoader(mod);
				
				if(loader == null)
					continue;
				
				mod.getLoadData().getLoaders().add(loader);
			}
		}
		
		for(ModInstance mod : mods.values()) {
			for(ModLoader loader : mod.getLoadData().getLoaders()) {
				loader.preload();
			}
		}
		
		DependencyManager<ModInstance> manager = new DependencyManager<ModInstance>();
		for(ModInstance mod : mods.values()) {
			Dependency<ModInstance> dependency = manager.wrap(mod.getConfig().name, mod);
			dependency.addAll(mod.getConfig().dependencies);
		}
		
		List<ModInstance> sortedMods = manager.getObjects();
		
		process(GlobalRegistry.REGISTRY.suppliers.createSupplier(), sortedMods);
		
		sendModLoaderListenerRegistryEvent(sortedMods);
		
		for(LoadEventSupplier<?> supplier : GlobalRegistry.REGISTRY.suppliers.getAll()) {
			process(supplier, sortedMods);
		}
	}
	
	
	private static void sendModLoaderListenerRegistryEvent(List<ModInstance> mods) {
		Logs.log(REGISTRY, "processing ModLoaderListenerRegistry");
		
		List<ModLoaderListenerRegistryEvent> events = new LinkedList<ModLoaderListenerRegistryEvent>();
		
		for(ModInstance mod : mods) {
			for(ModLoader loader : mod.getLoadData().getLoaders()) {
				events.add(new ModLoaderListenerRegistryEvent(loader));
			}
		}
		
		for(ModInstance mod : mods) {
			for(ModLoader loader : mod.getLoadData().getLoaders()) {
				for(ModLoaderListenerRegistryEvent event : events) {
					loader.getEventManager().send(event);
				}
			}
		}
	}
	
	private static void process(LoadEventSupplier<?> supplier, List<ModInstance> mods) {
		Logs.log(REGISTRY, "processing "+supplier);
		Logs.log(REGISTRY, "depends on:\n" + supplier.dependenciesToString());
		System.out.println("----------------------------");
		
		for(ModInstance mod : mods) {
			for(ModLoader loader : mod.getLoadData().getLoaders()) {
				loader.load(supplier);
			}
		}
		System.out.println("----------------------------");
	}
}
