package com.felixcool98.mods;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.felixcool98.events.EventManager;
import com.felixcool98.gdxutility.textures.TextureManager;
import com.felixcool98.mods.suppliers.LoadEventSupplier;

public interface ModLoader {
	public void preload();
	/**
	 * load via suppliers
	 * 
	 * @param registry
	 */
	public void load(LoadEventSupplier<?> supplier);
	/**
	 * the mod this loader belongs to
	 */
	public ModInstance getMod();
	
	public EventManager getEventManager();
	
	public FileHandle resolve(String path);
	public FileHandle resolve(FileHandle from, String path);
	
	public default TextureRegion resolveTexture(String path) {
		return TextureManager.INSTANCE.getTextureRegion(resolve(path));
	}
	public default TextureRegion resolveTexture(FileHandle from, String path) {
		return TextureManager.INSTANCE.getTextureRegion(resolve(from, path));
	}
}
