package dependencymanager;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.felixcool98.dependencies.Dependency;
import com.felixcool98.dependencies.DependencyManager;

class DependencyManagerTests {
	private static final String TREE_NAME = 		"com.felixcool98.spacegame.tree.Tree";
	private static final String ITEM_NAME = 		"com.felixcool98.spacegame.items.Item";
	private static final String BLOCK_NAME =		"com.felixcool98.spacegame.blocks.Block";
	private static final String ITEM_BLOCK_NAME = 	"ItemBlock";
	private static final String TEXTURE_LOADER_NAME = "TextureLoader";
	private static final String RECIPE_NAME = "com.felixcool98.spacegame.items.Recipe";
	private static final String RECIPE_TYPE_NAME = "com.felixcool98.spacegame.items.RecipeType";
	private static final String STATIC_OBJECT_NAME = "com.felixcool98.spacegame.staticobj.StaticObject";
	private static final String ITEM_COMPONENT_XML_READER_NAME = "ItemComponentXMLReader";
	
	
	@Test
	void test() {
		DependencyManager<String> manager = new DependencyManager<String>();
		
		Dependency<String> item = manager.wrap(ITEM_NAME, ITEM_NAME);
		item.add(ITEM_COMPONENT_XML_READER_NAME);
		
		Dependency<String> block = manager.wrap(BLOCK_NAME, BLOCK_NAME);
		block.add(TEXTURE_LOADER_NAME);
		
		Dependency<String> itemBlock = manager.wrap(ITEM_BLOCK_NAME, ITEM_BLOCK_NAME);
		itemBlock.add(ITEM_NAME);
		itemBlock.add(BLOCK_NAME);
		itemBlock.add(STATIC_OBJECT_NAME);
		
		manager.wrap(TEXTURE_LOADER_NAME, TEXTURE_LOADER_NAME);
		
		Dependency<String> recipe = manager.wrap(RECIPE_NAME, RECIPE_NAME);
		recipe.add(ITEM_NAME);
		recipe.add(RECIPE_TYPE_NAME);
		
		manager.wrap(RECIPE_TYPE_NAME, RECIPE_TYPE_NAME);
		
		manager.wrap(ITEM_COMPONENT_XML_READER_NAME, ITEM_COMPONENT_XML_READER_NAME);
		
		Dependency<String> staticObjCreator = manager.wrap("com.felixcool98.spacegame.ingame.StaticObjectCreator", "com.felixcool98.spacegame.ingame.StaticObjectCreator");
		staticObjCreator.add(TEXTURE_LOADER_NAME);
		
		manager.wrap("com.felixcool98.spacegame.world.WorldGenerationTask", "com.felixcool98.spacegame.world.WorldGenerationTask");
		
		Dependency<String> ore = manager.wrap("com.felixcool98.spacegame.ore.Ore", "com.felixcool98.spacegame.ore.Ore");
		ore.add(ITEM_NAME);
		
		Dependency<String> staticObject = manager.wrap(STATIC_OBJECT_NAME, STATIC_OBJECT_NAME);
		staticObject.add(TEXTURE_LOADER_NAME);
		
		Dependency<String> tree = manager.wrap(TREE_NAME, TREE_NAME);
		tree.add(STATIC_OBJECT_NAME);
		tree.add(BLOCK_NAME);
		
		Dependency<String> worldGenerator = manager.wrap("com.felixcool98.spacegame.world.WorldGenerator", "com.felixcool98.spacegame.world.WorldGenerator");
		worldGenerator.add(BLOCK_NAME);
		worldGenerator.add("com.felixcool98.spacegame.ore.Ore");
		worldGenerator.add(STATIC_OBJECT_NAME);
		
		manager.wrap("GUIMenu", "GUIMenu");
		
		
		
		List<Dependency<String>> l = manager.getAll();
		
		for(Dependency<String> d : l) {
			System.out.println(d.getName());
			System.out.println(d.getDependencies());
		}
		
		System.out.println("-------------");
		
		List<String> done = new LinkedList<String>();
		
		for(String str : manager.getObjects()) {
			System.out.println(str);
			System.out.println(manager.get(str).getDependencies());
			
			for(String dp : manager.get(str).getDependencies()) {
				if(!done.contains(dp))
					fail();
			}
			
			done.add(str);
		}
	}
}
