package com.felixcool98.dependencies;

import java.util.LinkedList;
import java.util.List;

public class Dependency<T> {
	private T object;
	
	private String name;
	
	private List<String> dependencies = new LinkedList<String>();
	
	private int priority;
	
	
	public Dependency(String name, T object) {
		this.name = name;
		this.object = object;
	}
	
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getPriority() {
		return priority;
	}
	
	
	public void add(Dependency<?> dependency) {
		add(dependency.getName());
	}
	public void addAll(String[] dependencies) {
		for(String dependency : dependencies) {
			add(dependency);
		}
	}
	public void add(String dependency) {
		dependencies.add(dependency);
	}
	
	public List<String> getDependencies() {
		return dependencies;
	}
	
	public boolean dependsOn(Dependency<?> dependency, DependencyManager<T> manager) {
		return dependsOn(dependency.getName(), manager);
	}
	/**
	 * @param dependency
	 * @param manager
	 * @return true if this dependency or any of it's dependencies depends on the the given name
	 */
	public boolean dependsOn(String dependency, DependencyManager<T> manager) {
		for(String name : getDependencies()) {
			if(name.equals(dependency))
				return true;
			
			Dependency<T> d = manager.get(name);
			
			if(d == null)
				throw new DependencyNotFoundException("couldn't find "+name+" for "+getName());
			
			if(d.dependsOn(dependency, manager))
				return true;
		}
		
		return false;
	}
	
	public boolean hasDependencies() {
		return !dependencies.isEmpty();
	}
	
	public String getName() {
		return name;
	}
	public T getObject() {
		return object;
	}
	
	@Override
	public String toString() {
		return getObject().toString();
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Dependency))
			return false;
		
		return name.equals(((Dependency<?>)obj).name);
	}

	
	public void checkForCircle(DependencyManager<T> manager) {
		checkForCircle(this, manager);
	}
	private void checkForCircle(Dependency<?> src, DependencyManager<T> manager) {
		for(String d : getDependencies()) {
			if(d.equals(src.getName()))
				throw CircleExeption.get(d, this.getName());
			
			if(manager.get(d).hasDependencies()) {
				try {
					manager.get(d).checkForCircle(src, manager);
				}catch (CircleExeption e) {
					throw CircleExeption.get(e, this.getName());
				}
			}
		}
	}
}
