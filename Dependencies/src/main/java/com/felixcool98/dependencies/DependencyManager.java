package com.felixcool98.dependencies;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DependencyManager<T> {
	private Map<String, Dependency<T>> dependencies = new HashMap<String, Dependency<T>>();
	private List<Dependency<T>> failed = new ArrayList<Dependency<T>>();
	
	private boolean verified = false;
	
	public final Comparator<Dependency<T>> comparator = (obj, other)->{
		if(!obj.hasDependencies() && !other.hasDependencies())//if both have no dependencies order by name
			return obj.getName().compareTo(other.getName());
		if(!obj.hasDependencies())//if obj1 has no dependencies put obj1 in front of obj2
			return -1;
		if(!other.hasDependencies())//if obj2 has no dependencies put obj1 behind obj2
			return 1;
		if(obj.dependsOn(other, this))//if obj1 depends on obj2
			return 1;
		if(other.dependsOn(obj, this))//if obj2 depends on obj1
			return -1;
		if(obj.getDependencies().size() != other.getDependencies().size())
			return obj.getDependencies().size()-other.getDependencies().size();
		if(obj.getPriority() == other.getPriority())
			return obj.getName().compareTo(other.getName());
		
		return obj.getPriority() - other.getPriority();
	};
	
	
	public Dependency<T> wrap(String name, T object){
		Dependency<T> dependency = new Dependency<T>(name, object);
		
		dependencies.put(dependency.getName(), dependency);
		
		verified = false;
		
		return dependency;
	}
	
	
	public Dependency<T> get(String name){
		return dependencies.get(name);
	}
	
	
	private Stream<Dependency<T>> sortedStream(){
		verify();
		
		return this.dependencies.values().stream().sorted(comparator).map(t ->{
			t.checkForCircle(this);
			
			return t;
		});
	}
	public List<Dependency<T>> getAll(){
		return sortedStream()
				.collect(Collectors.toList());
	}
	/**
	 * @return all objects nested in the dependencies already verified and sorted
	 */
	public List<T> getObjects(){
		return sortedStream()
				.map(Dependency::getObject)
				.collect(Collectors.toList());
	}
	
	
	public void verify() {
		if(verified)
			return;
		
		for(Dependency<T> dependency : dependencies.values().stream().sorted(comparator).collect(Collectors.toList())) {
			verify(dependency);
		}
		
		verified = true;
	}
	public void verify(Dependency<T> dependency) {
		boolean remove = false;
		
		for(String name : dependency.getDependencies()) {
			if(!dependencies.containsKey(name)) {
				System.err.println("couldn't resolve "+name+" is it not registered?");
				
				remove = true;
			}
		}
		
		if(remove) {
			dependencies.remove(dependency.getName());
			failed.add(dependency);
			System.err.println("removed "+dependency+" because some dependencies couldn't be resolved");
		}
	}
	public List<Dependency<T>> getFailed() {
		return failed;
	}
}
