package com.felixcool98.dependencies;

public class DependencyNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 2874765160813405476L;
	

	public DependencyNotFoundException(String message) {
		super(message);
	}
}
