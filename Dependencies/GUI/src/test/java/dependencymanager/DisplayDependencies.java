package dependencymanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.dependencies.Dependency;
import com.felixcool98.dependencies.DependencyManager;
import com.felixcool98.dependencies.gui.DependencyTable;
import com.felixcool98.game.Game;
import com.felixcool98.game.GameStateManager;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.launchutils.lwjgl3.LaunchUtilsLwjgl3;
import com.kotcrab.vis.ui.widget.VisLabel;

public class DisplayDependencies extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setWindowedMode(1028, 720);
		config.setResizable(false);
		
		LaunchUtilsLwjgl3.launch(new Game() {
			@Override
			protected ApplicationListener registerGameStates(GameStateManager manager) {
				return new DisplayDependencies();
			}
		}, config);
	}
	
	@Override
	public void created() {
		setClearColor(Color.BLACK);
		
		DependencyManager<String> manager = new DependencyManager<String>();
		
		Dependency<String> recipe = manager.wrap("recipe", "recipe");
		
		Dependency<String> item = manager.wrap("item", "item");
		recipe.add("item");
		
		Dependency<String> block = manager.wrap("block", "block");
		Dependency<String> blockDrop = manager.wrap("blockDrop", "blockDrop");
		blockDrop.add(block);
		blockDrop.add(item);
		
		Dependency<String> other = manager.wrap("other", "other");
		other.add(blockDrop);
		
		List<Dependency<String>> dependencies = manager.getAll();
		
		List<Dependency<String>> process = new ArrayList<Dependency<String>>(dependencies);
		List<String> processed = new ArrayList<String>(dependencies.size());
		
		List<List<Dependency<String>>> layers = new LinkedList<List<Dependency<String>>>();
		
		while(!process.isEmpty()) {
			List<Dependency<String>> done = new LinkedList<Dependency<String>>();
			
			for(Dependency<String> dependency : process) {
				if(!dependency.hasDependencies()) {
					done.add(dependency);
				}else {
					boolean skip = false;
					
					for(String needed : dependency.getDependencies()) {
						if(!processed.contains(needed)) {
							skip = true;
							break;
						}
					}
					
					if(!skip) 
						done.add(dependency);
				}
			}
			
			for(Dependency<String> dependency : done) {
				processed.add(dependency.getName());
				process.remove(dependency);
			}
			
			layers.add(done);
		}
		
		Map<String, DependencyTable> tables = new HashMap<String, DependencyTable>();
		
		for(Dependency<String> dependency : dependencies) {
			DependencyTable table = new DependencyTable();
			
			table.add(new VisLabel(dependency.getName())).pad(5);
			
			tables.put(dependency.getName(), table);
		}
		
		for(Dependency<String> dependency : dependencies) {
			DependencyTable table = tables.get(dependency.getName());
			
			for(String d : dependency.getDependencies()) {
				table.dependsOn(tables.get(d));
			}
		}
		
		Table table = new Table();{
			table.align(Align.bottomLeft);
			
			for(int i = 0; i < layers.size(); i++) {
				Table layer = new Table();{
					layer.align(Align.top);
					
					for(Dependency<String> dependency : layers.get(i)) {
						layer.add(tables.get(dependency.getName()))
							.pad(5)
							.align(Align.top)
							.fillX()
							.expandX()
							.row();
					}
				}table.add(layer).align(Align.top).pad(5);
				System.out.println(layer);
			}
		}getStage().addActor(table);
	}
}
