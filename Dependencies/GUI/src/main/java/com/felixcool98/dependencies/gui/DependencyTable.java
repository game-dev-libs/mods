package com.felixcool98.dependencies.gui;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.widget.VisTable;

public class DependencyTable extends VisTable {
	private ShapeRenderer renderer = new ShapeRenderer();
	
	private List<Actor> dependsOn = new LinkedList<Actor>();
	
	
	public void dependsOn(Actor actor) {
		dependsOn.add(actor);
	}
	
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		validate();
		
		batch.end();
		renderer.begin(ShapeType.Line);
		
		for(Actor actor : dependsOn) {
			Vector2 otherCoords = actor.localToStageCoordinates(new Vector2(actor.getWidth(), actor.getHeight()/2f));
			Vector2 coords = localToStageCoordinates(new Vector2(0, getHeight()/2f));
			
			float xDiff = coords.x - otherCoords.x;
			float yDiff = coords.y - otherCoords.y;
			
			renderer.curve(otherCoords.x, otherCoords.y,
							coords.x, otherCoords.y,
							otherCoords.x, coords.y,
							coords.x, coords.y, 
							Math.max(10, (int)(Math.sqrt(Math.abs(xDiff*yDiff)))));
			
			renderer.end();
			renderer.setColor(Color.BLUE);
			renderer.begin(ShapeType.Filled);{
				renderer.circle(otherCoords.x, otherCoords.y, 4);
			}renderer.end();
			renderer.begin(ShapeType.Line);
			renderer.setColor(Color.WHITE);
		}
		renderer.rect(getX(), getY(), getWidth(), getHeight());
		renderer.end();
		batch.begin();
		
		super.draw(batch, parentAlpha);
	}
}
